## middlewares

> src/middlewares

In this folder, you will store all your middlewares. The purpose of a middleware is to extract a common controller code, which should be executed on multiple requests and usually modifies the request and/or the response objects.
