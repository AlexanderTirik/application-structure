## helpers

> src/helpers

In this folder, we must store the helper functions. Helper functions make complicated or repetitive tasks a bit easier, and keep your code DRY. We can use them in any other part of the code.
