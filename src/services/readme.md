## services

> src/services

In the `services` folder, we will store all the services that serve our application. Each service, independently of the other, must respond to controller requests by manipulating data using repositories.