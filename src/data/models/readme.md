## models

> src/data/models

`models` folder contains model class files. Typically model class includes public properties, which will be used by application to hold and manipulate application data.