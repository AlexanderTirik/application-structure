## repositories

> src/data/repositories

This folder holds all the repositories. A repository is a collection. A collection that contains entities and can filter and return the result back depending on the requirements of ours application.
