## Data

> src/data

This folder is designed to store everything related to the database. This is: `migrations`, `models`, `db`, `repositories`. There may also be other folders associated with the database, it all depends on your technology stack.
