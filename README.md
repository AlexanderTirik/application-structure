# WakePark Project


## Goal

The goal of this project is to practice software architecture building skills.

## Architecture

In this project, my choice fell on a microservice architecture. The main focus was on the comparison between monolithic architecture. But in this project, as for me, microservice is more suitable.

## Why this architecture?

Microservices provide the ideal architecture for continuous delivery. With microservices, each application resides in a separate container along with the environment it needs to run. Because of this, each application can be edited in its container without the risk of interfering with any other application. For example, after the release of the program, the customer decides to build a sauna on the territory of his park, and also asks us to implement additional functionality in the application. How are we going to scale it in monolithic architecture? We will do this for a very long time and tediously, for this there are microservices. This architecture can also scale to other platforms such as chatbots.

## What about DDD?

This project is closely related to real business, so the connection between the program and the business should be as close as possible. The code should be readable even by the top managers of the park. This is necessary so that in the event of a problem, you can quickly understand and navigate what needs to be corrected in order to solve this problem. Therefore, each project programmer should adhere to this design, and think 100 times before naming a variable.

## Which protocol should use?

In this project, we do not have such cases where the user needs to open a direct channel between the client and the server, so WebSockets is not our choice. My choice fell on HTTP/REST because the microservice architecture is well organized for these requests. The HTTP/REST communication is ideal for external requests, as it’s built to easily handle items like real-time interactions from a client.